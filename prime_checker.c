#include <stdio.h>

int is_prime(unsigned long n) {
    if (n <= 1) {
        return 0;
    }
    if (n == 2) {
        return 1;
    }
    if (n % 2 == 0) {
        return 0;
    }
    for (unsigned long i = 3; i*i < n; i+=2) {
        if (n % i == 0) {
            return 0;
        }
    }
    return 1;
}

int main() {
    unsigned long n;
    scanf("%lu", &n);
    printf("%d\n", is_prime(n));
    return 0;
}

