#include <stdio.h>

const int arr1[3] = {1, 2, 3};
const int arr2[3] = {4, 5, 6};

int scalar_product(const int* first_array, const int* second_array, size_t length) {
    int sum = 0;
    for (size_t i = 0; i < length ; i++) {
        sum += first_array[i] * second_array[i];
    }
    return sum;
}

int main() {
    size_t length = sizeof(arr1) / sizeof(int);
    printf("The scalar product of vectors is: %d\n", scalar_product(arr1, arr2, length));
    return 0;
}
