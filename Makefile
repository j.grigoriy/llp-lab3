prime_checker:
	gcc -o prime_checker prime_checker.c

scalar_product:
	gcc -o scalar_product scalar_product.c

run_prime_checker: prime_checker
	./prime_checker

run_scalar_product: scalar_product
	./scalar_product

clean:
	rm -rf scalar_product
	rm -rf prime_checker
